package com.groundgurus.fp.factory;

import com.groundgurus.fp.model.Car;
import com.groundgurus.fp.model.Chevrolet;
import com.groundgurus.fp.model.Ferarri;
import com.groundgurus.fp.model.MercedesBenz;

public class CarFactory {
    public Car create(String brand) {
        switch (brand) {
            case "Mercedez Benz":
                return new MercedesBenz();
            case "Chevolet":
                return new Chevrolet();
            case "Ferrari":
                return new Ferarri();
            default:
                throw new RuntimeException("Unable to create brand " + brand);
        }
    }
}
