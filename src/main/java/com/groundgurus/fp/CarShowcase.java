package com.groundgurus.fp;

import com.groundgurus.fp.factory.CarFactory;

public class CarShowcase {
    public static void main(String[] args) {
        CarFactory myCarFactory = new CarFactory();

        System.out.println(myCarFactory.create("Ferrari"));
    }
}
